class CreateChapters < ActiveRecord::Migration
  def change
    create_table :chapters do |t|
      t.string :chapterNumber
      t.string :title
      t.string :poster
      t.string :synopsis
      t.integer :stars
      t.integer :price
      t.string :url_chapter
      t.string :url_trailer
      t.belongs_to :season
      t.timestamps null: false
    end
  end
end
