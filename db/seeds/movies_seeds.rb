# Based on http://www.imdb.com/chart/top?ref_=ft_250
Movie.create([
{
 title: 'QFlix Screencast - Release 2',
 year: '2016',
 synopsis: 'Presentacion final del funcionamiento de la aplicacion QFlix - Release 2',
 poster: '34.jpg',
 genre: 'Programación',
 stars: 5,
 price: 1000,
 url_movie: '_95tqF00r7Y',
 url_trailer: '_95tqF00r7Y'
},{
 title: 'QFlix Screencast - Release 1',
 year: '2016',
 synopsis: 'Presentacion de funcionamiento de la aplicacion QFlix - Release 1',
 poster: '22.jpg',
 genre: 'Programación',
 stars: 5,
 price: 10,
 url_movie: 'SjYarYlMOUs',
 url_trailer: 'OI6ZlbXde-A'
},{
 title: 'Sueños de libertad',
 year: '1994',
 synopsis: 'Dos hombres encarcelados durante varios años buscan consuelo y eventual redención a través de actos de la decencia común.',
 poster: '1.jpg',
 genre: 'Drama',
 stars: 3,
 price: 10,
 url_movie: 'raRbQkJ2pD4',
 url_trailer: 'raRbQkJ2pD4'
},{
 title: 'El padrino',
 year: '1972',
 synopsis: 'Cuando el lider de una familia, famosa por sus crimenes, decide ceder su posicion a su hederero, una serie de eventos desafortunados comienzan a suceder y una guerra comienza entre varias las familias conocidas que lleva a la insolencia, la expulsión, el asesinato y venganza.',
 poster: '2.jpg',
 genre: 'Drama',
 stars: 2,
 price: 6,
 url_movie: 'COQvkUmN6H8',
 url_trailer: 'COQvkUmN6H8'
},{
 title: 'El padrino II',
 year: '1974',
 synopsis: 'La saga de la famosa familia mafiosa continua con el crecicimiento de Vito Corleone en Sicilia y en la decada de 1910 en Nueva York. Mientras que su hijo, Michael Corleone, intenta expandir el negocio familiar por Las Vegas, Hollywood y Cuba',
 poster: '3.jpg',
 genre: 'Drama',
 stars: 4,
 price: 6,
 url_movie: '2X4hIriy8U8',
 url_trailer: '2X4hIriy8U8'
},{
 title: 'Batman - El caballero de la noche',
 year: '2008',
 synopsis: 'Cuando el asesino conocido como Joker causa caos en las personas de Ciudad Gotica, Batman debe llegar a un acuerdo en una de sus mayores pruebas psicologicas contra la injusticia.',
 poster: '4.jpg',
 genre: 'Aventura',
 stars: 5,
 price: 15,
 url_movie: '6aKzEhdCcwo',
 url_trailer: '6aKzEhdCcwo'
},{
 title: 'La lista de Schindler ',
 year: '1993',
 synopsis: 'En Polonia durante la Segunda Guerra Mundial , Oskar Schindler decide convertir su fabrica en un refugio para Judios después de haber sidos perseguidos por los nazis',
 poster: '5.jpg',
 genre: 'Drama',
 stars: 2,
 price: 6,
 url_movie: 'wsIQIPmiAuc',
 url_trailer: 'wsIQIPmiAuc'
},{
 title: 'Tiempos violentos',
 year: '1994',
 synopsis: 'The lives of two mob hit men, a boxer, a gangster\'s wife, and a pair of diner bandits intertwine in four tales of violence and redemption.',
 poster: '6.jpg',
 genre: 'Drama',
 stars: 1,
 price: 8,
 url_movie: 'DkgTIjkVrY8',
 url_trailer: 'DkgTIjkVrY8'
},{
 title: 'El señor de los anillos: El retorno del rey',
 year: '2003',
 synopsis: 'Gandalf and Aragorn lideran el mundo de los hombre contra el ejecito de Sauron para alejar la mirada de Frodo y Sam que cabe vez estan mas cerca del Monte del Destino para destruir el anillo ',
 poster: '7.jpg',
 genre: 'Aventura',
 stars: 2,
 price: 9,
 url_movie: 'h-9RYiqyqjk',
 url_trailer: 'h-9RYiqyqjk'
},{
 title: 'El club de la pelea',
 year: '1999',
 synopsis: 'Un empleado de oficina, buscando cambiar su vida, se cruza con arriesgado fabricante de jabones, formando un club de lucha subterranea que se convierte en algo mucho, mucho mas...',
 poster: '8.jpg',
 genre: 'Drama',
 stars: 5,
 price: 12,
 url_movie: 'SUXWAEX2jlg',
 url_trailer: 'SUXWAEX2jlg'
},{
 title: 'El señor de los anillos: La comunidad del anillo',
 year: '2001',
 synopsis: 'Un hobbit y 8 compañeros elegidos emprenden un viaje para destruir el anillo de poder y al Señor Sauron.',
 poster: '9.jpg',
 genre: 'Aventura',
 stars: 4,
 price: 5,
 url_movie: '3GJp6p_mgPo',
 url_trailer: '3GJp6p_mgPo'
},{
 title: 'Star Wars: Episodio V - El imperio contraataca',
 year: '1980',
 synopsis: 'Después de que los rebeldes han sido brutalmente dominados por el Imperio, Luke Skywalker avanza en su entrenamiento Jedi con el Maestro Yoda, mientras que sus amigos son perseguidos por Darth Vader como parte de su plan para capturar a Luke. ',
 poster: '10.jpg',
 genre: 'Aventura',
 stars: 2,
 price: 6,
 url_movie: 'xr3hPFJAHO4',
 url_trailer: 'xr3hPFJAHO4'
},{
 title: 'Forrest Gump',
 year: '1994',
 synopsis: 'Forrest Gump, sin ser muy inteligente, ha estado accidentalmente presente en muchos momentos históricos, pero su verdadero amor, Jenny Curran, lo elude.',
 poster: '11.jpg',
 genre: 'Drama',
 stars: 1,
 price: 4,
 url_movie: '3lOZG6YkWi4',
 url_trailer: '3lOZG6YkWi4'
},{
 title: 'El origen',
 year: '2010',
 synopsis: 'Un ladrón que roba secretos corporativos a través del uso de la tecnología de intercambio del sueño, se dio a la tarea inversa de plantar una idea en la mente de un CEO.',
 poster: '12.jpg',
 genre: 'Accion',
 stars: 5,
 price: 14,
 url_movie: 'RyfbchpdJ6U',
 url_trailer: 'RyfbchpdJ6U'
},{
 title: 'El señor de los anillos: Las dos torres',
 year: '2002',
 synopsis: 'Mientras Frodo y Sam se acercan más a Mordor con la ayuda de Gollum, la comunidad dividida se mantiene firme contra el nuevo aliado de Sauron, Saruman, y sus hordas de Isengard.',
 poster: '13.jpg',
 genre: 'Aventura',
 stars: 1,
 price: 3,
 url_movie: 'yZLxtW7qq48',
 url_trailer: 'yZLxtW7qq48'
},{
 title: 'Atrapado sin salida',
 year: '1975',
 synopsis: ' R.P. McMurphy, un hombre que cumple una pena en prisión acusado de haber tenido relaciones sexuales con una menor de edad, con una astucia tal que es capaz de convencer al juez que lleva su caso de que tiene un trastorno mental, por lo cual es llevado a un hospital psiquiátrico. Durante su estancia en el hospitalconoce varios enfermos mentales y planea una fuga del psiquiátrico.',
 poster: '14.jpg',
 genre: 'Drama',
 stars: 3,
 price: 0,
 url_movie: '1qMFh22qGyg',
 url_trailer: '1qMFh22qGyg'
},{
 title: 'Buenos muchachos',
 year: '1990',
 synopsis: 'Henry Hill y sus amigos deciden abrirse camino dentro de la mafia.',
 poster: '15.jpg',
 genre: 'Drama',
 stars: 2,
 price: 4,
 url_movie: '2uYmEFnu8Y8',
 url_trailer: '2uYmEFnu8Y8'
},{
 title: 'Matrix',
 year: '1999',
 synopsis: 'Un pirata informático aprende de misteriosos rebeldes sobre la verdadera naturaleza de su realidad y su papel en la guerra contra sus controladores.',
 poster: '16.jpg',
 genre: 'Ciencia Ficción',
 stars: 1,
 price: 6,
 url_movie: 'm8e-FF8MsqU',
 url_trailer: 'm8e-FF8MsqU'
},{
 title: 'Los siete samuráis',
 year: '1954',
 synopsis: 'Un pueblo pobre bajo el ataque de bandidos recluta a siete samuráis en paro para ayudar a defenderse.',
 poster: '17.jpg',
 genre: 'Drama',
 stars: 1,
 price: 0,
 url_movie: '6DGj8EqWJyU',
 url_trailer: '6DGj8EqWJyU'
},{
 title: 'Star Wars: Episodio IV',
 year: '1977',
 synopsis: 'Luke Skywalker une fuerzas con un caballero jedi, un piloto engreído, un wookie y dos droides para salvar la galaxia de la estación de batalla del Imperio, y al mismo tiempo tratar de rescatar a la princesa Leia de Darth Vader.',
 poster: '18.jpg',
 genre: 'Aventura',
 stars: 2,
 price: 2,
 url_movie: '9gvqpFbRKtQ',
 url_trailer: '9gvqpFbRKtQ'
},{
 title: 'Ciudad de Dios',
 year: '2002',
 synopsis: 'Dos niños que crecen en un barrio violento de Río de Janeiro toman caminos diferentes: uno se convierte en un fotógrafo, el otro un vendedor de drogas.',
 poster: '19.jpg',
 genre: 'Drama',
 stars: 5,
 price: 3,
 url_movie: 'zyzJj9lF1PU',
 url_trailer: 'zyzJj9lF1PU'
},{
 title: 'Pecados capitales',
 year: '1995',
 synopsis: 'Dos detectives, un novato y un veterano, cazan un asesino en serie que utiliza los siete pecados capitales como su modus operandi.',
 poster: '20.jpg',
 genre: 'Drama',
 stars: 4,
 price: 1,
 url_movie: 'q9BOtSBq-00',
 url_trailer: 'q9BOtSBq-00'
},{
 title: 'El silencio de los inocentes',
 year: '1991',
 synopsis: 'Un joven F.B.I. cadete debe confiar en un asesino encarcelado y manipulador para recibir su ayuda en la captura de otro asesino en serie que despelleja a sus víctimas.',
 poster: '21.jpg',
 genre: 'Drama',
 stars: 5,
 price: 2,
 url_movie: '0n49lk8jJjw',
 url_trailer: '0n49lk8jJjw'
},
])