Chapter.create([
{
    chapterNumber: '1x01',
    title: 'A Study in Pink',
    poster: '26.jpg',
    synopsis: 'El veterano de guerra, Dr. John Watson vuelve a Londres en necesidad de un lugar para quedarse. Conoce a Sherlock Holmes, detective de consultoría, y los dos pronto se encuentran excavando en una cadena de suicidios en serie',
    stars: 5,
    price: 10,
    url_chapter: '704AblLNlY8',
    url_trailer: '704AblLNlY8',
    season_id: @shseason1.id
},{
    chapterNumber: '1x02',
    title: 'The Blind Banker',
    poster: '26.jpg',
    synopsis: 'Símbolos misteriosos y asesinatos están apareciendo por todo Londres, lo que lleva a Sherlock y John a una asociación secreta del crimen chino, llamada Lotus Negro.',
    stars: 4,
    price: 10,
    url_chapter: 'AmyaXPjnas0',
    url_trailer: 'AmyaXPjnas0',
    season_id: @shseason1.id
},{
    chapterNumber: '1x03',
    title: 'The Great Game',
    poster: '26.jpg',
    synopsis: 'Mycroft necesita la ayuda de Sherlock, pero un cerebro criminal implacable pone a Sherlock en una cadena de crímenes de resolución de distracción a través de una serie de bombas de rehenes humanos a través de los que habla.',
    stars: 5,
    price: 10,
    url_chapter: 'XNbT1u6uKq8',
    url_trailer: 'XNbT1u6uKq8',
    season_id: @shseason1.id
},{
    chapterNumber: '2x01',
    title: 'A Scandal in Belgravia',
    poster: '27.jpg',
    synopsis: 'Sherlock debe confiscar algo de importancia a partir de una misteriosa mujer llamada Irene Adler.',
    stars: 5,
    price: 10,
    url_chapter: '7PhzRmUD4mI',
    url_trailer: '7PhzRmUD4mI',
    season_id: @shseason2.id
},{
    chapterNumber: '2x02',
    title: 'The Hounds of Baskerville',
    poster: '27.jpg',
    synopsis: 'Sherlock y John investigan los fantasmas de un joven que ha estado viendo perros monstruosos en el bosque, donde murió su padre.',
    stars: 4,
    price: 10,
    url_chapter: '4lUDyBPPsGU',
    url_trailer: '4lUDyBPPsGU',
    season_id: @shseason2.id
},{
    chapterNumber: '2x03',
    title: 'The Reichenbach Fall',
    poster: '27.jpg',
    synopsis: 'Jim Moriarty trama un esquema de locos para convertir toda la ciudad contra Sherlock.',
    stars: 5,
    price: 10,
    url_chapter: 'wVoo9RqrHtw',
    url_trailer: 'wVoo9RqrHtw',
    season_id: @shseason2.id
},{
    chapterNumber: '3x00',
    title: 'Many Happy Returns',
    poster: '28.jpg',
    synopsis: 'John y Lestrade tratan de seguir adelante con sus vidas después de la aparente muerte de Sherlock. Sin embargo, Anderson cree que todavía está vivo.',
    stars: 4,
    price: 0,
    url_chapter: 'JwntNANJCOE',
    url_trailer: 'JwntNANJCOE',
    season_id: @shseason3.id
},{
    chapterNumber: '3x01',
    title: 'The Empty Hearse',
    poster: '28.jpg',
    synopsis: 'Mycroft llama a Sherlock para volver a Londres, a investigar una organización terrorista subterránea.',
    stars: 5,
    price: 10,
    url_chapter: 'O7cKIjNIPoY',
    url_trailer: 'O7cKIjNIPoY',
    season_id: @shseason3.id
},{
    chapterNumber: '3x02',
    title: 'The Sign of Three',
    poster: '28.jpg',
    synopsis: 'Sherlock trata de dar el discurso perfecto de padrino en la boda de John, cuando se da cuenta de un asesinato está a punto de tener lugar.',
    stars: 5,
    price: 10,
    url_chapter: 'N-lMgIg2p1c',
    url_trailer: 'N-lMgIg2p1c',
    season_id: @shseason3.id
},{
    chapterNumber: '3x03',
    title: 'His Last Vow',
    poster: '28.jpg',
    synopsis: 'Sherlock se enfrenta al notorio chantajista, Charles Augustus Magnussen.',
    stars: 5,
    price: 10,
    url_chapter: 'LI7T69cA3Kg',
    url_trailer: 'LI7T69cA3Kg',
    season_id: @shseason3.id
},{
    chapterNumber: '4x00',
    title: 'The Abominable Bride',
    poster: '25.jpg',
    synopsis: 'Sherlock Holmes y el Dr. Watson se encuentran en Londres 1890 en este especial de vacaciones.',
    stars: 4,
    price: 10,
    url_chapter: '5nJbr3XpppA',
    url_trailer: '5nJbr3XpppA',
    season_id: @shseason4.id
}
])


Chapter.create([
{
    chapterNumber: '1x01',
    title: 'The Long Bright Dark',
    poster: '24.jpg',
    synopsis: 'En 2012, la ex pareja de detectives, Rust Cohle y Martin Hart recapitulan uno de sus primeros casos en conjunto, el que implicaba a un asesino en serie, en el año 1995.',
    stars: 5,
    price: 10,
    url_chapter: 'D-7FtdUkv-A',
    url_trailer: 'D-7FtdUkv-A',
    season_id: @tdseason1.id
},{
    chapterNumber: '1x02',
    title: 'Seeing Things',
    poster: '24.jpg',
    synopsis: 'Cohle y Hart viajan tratando de localizar pistas en su caso. Se revelan lados más oscuros de la vida personal y familiar de Hart.',
    stars: 4,
    price: 10,
    url_chapter: 'dqPQYcqpNHE',
    url_trailer: 'dqPQYcqpNHE',
    season_id: @tdseason1.id
},{
    chapterNumber: '1x03',
    title: 'The Locked Room',
    poster: '24.jpg',
    synopsis: 'Cohle y Martin finalmente consiguen una pista nueva en el caso y pueden confirmar a un sospechoso. La amante de Martin comienza a ver a otra persona, dejándolo furioso. La teoría de Cohle se vuelve más convincente que nunca.',
    stars: 5,
    price: 10,
    url_chapter: 'XASlxCvbKGo',
    url_trailer: 'XASlxCvbKGo',
    season_id: @tdseason1.id
},{
    chapterNumber: '1x04',
    title: 'Who Goes There',
    poster: '24.jpg',
    synopsis: 'Martin se enfrenta a problemas conyugales. La búsqueda de Reggie Ledoux lleva a los detectives a una banda de motociclistas llamada The Iron Crusaders, en la cual Rust trabajo encubierto en el pasado.',
    stars: 5,
    price: 10,
    url_chapter: 'zpfMjxlSuus',
    url_trailer: 'zpfMjxlSuus',
    season_id: @tdseason1.id
},{
    chapterNumber: '1x05',
    title: 'The Secret Fate of All Life',
    poster: '24.jpg',
    synopsis: 'Hart y Cohle comparten el botín de un caso resuelto; Papania y Gilbough enfrentan a los detectives con nueva inteligencia preocupante.',
    stars: 5,
    price: 10,
    url_chapter: 'C43MGGkzShA',
    url_trailer: 'C43MGGkzShA',
    season_id: @tdseason1.id
},{
    chapterNumber: '1x06',
    title: 'Haunted Houses',
    poster: '24.jpg',
    synopsis: 'Las actividades de Cohle en 2002, cuando su asociación con Hart se desintegró y dejó la fuerza, son recordadas por Maggie.',
    stars: 5,
    price: 10,
    url_chapter: 'ZSZ3dOCH5Vg',
    url_trailer: 'ZSZ3dOCH5Vg',
    season_id: @tdseason1.id
},{
    chapterNumber: '1x07',
    title: 'After You have Gone',
    poster: '24.jpg',
    synopsis: 'Cohle y Hart tratan de descubrir si una serie de personas desaparecidas podría estar relacionado con el asesinato de Dora Lange y la familia Tuttle.',
    stars: 5,
    price: 10,
    url_chapter: 'ZBvXKe-HMhA',
    url_trailer: 'ZBvXKe-HMhA',
    season_id: @tdseason1.id
},{
    chapterNumber: '1x08',
    title: 'Form and Void',
    poster: '24.jpg',
    synopsis: 'Un detalle pasado por alto proporciona a Hart y Cohle con una nueva e importante pista en su caso de 17 años.',
    stars: 5,
    price: 10,
    url_chapter: 'PORrGrQrFtk',
    url_trailer: 'PORrGrQrFtk',
    season_id: @tdseason1.id
},{
    chapterNumber: '2x01',
    title: 'The Western Book of the Dead',
    poster: '24.jpg',
    synopsis: 'Un extraño asesinato reúne a tres oficiales de policía y un criminal de carrera en la ciudad corrupta de Vinci, California.',
    stars: 4,
    price: 10,
    url_chapter: 'LDQtjYJAk3k',
    url_trailer: 'LDQtjYJAk3k',
    season_id: @tdseason2.id
},{
    chapterNumber: '2x02',
    title: 'Night Finds You',
    poster: '24.jpg',
    synopsis: 'Ray, Ani, y Paul son asignados al caso del asesinato de Casper. Frank intenta mantener sus negocios en marcha, incluso después de la muerte de Casper.',
    stars: 4,
    price: 10,
    url_chapter: 'NlZUTgN9qGs',
    url_trailer: 'NlZUTgN9qGs',
    season_id: @tdseason2.id
},{
    chapterNumber: '2x03',
    title: 'Maybe Tomorrow',
    poster: '24.jpg',
    synopsis: 'Paul trabaja el ángulo de prostituta; Frank recibe la primera víctima de una guerra secreta.',
    stars: 5,
    price: 10,
    url_chapter: 'bNDvYZL0f8A',
    url_trailer: 'bNDvYZL0f8A',
    season_id: @tdseason2.id
},{
    chapterNumber: '2x04',
    title: 'Down Will Come',
    poster: '24.jpg',
    synopsis: 'El trabajo detallista de casa de empeño lleva a obtener a un sospechoso en el caso Casper. Frank vuelve a su pasado para pagar por su presente.',
    stars: 4,
    price: 10,
    url_chapter: 'AA2lucgdxo4',
    url_trailer: 'AA2lucgdxo4',
    season_id: @tdseason2.id
},{
    chapterNumber: '2x05',
    title: 'Other Lives',
    poster: '24.jpg',
    synopsis: 'Ray y Frank consideran nuevas opciones de vida; Ani y Paul van hasta la costa para seguir una pista.',
    stars: 4,
    price: 10,
    url_chapter: 's-rmdttg3EI',
    url_trailer: 's-rmdttg3EI',
    season_id: @tdseason2.id
},{
    chapterNumber: '2x06',
    title: 'Church in Ruins',
    poster: '24.jpg',
    synopsis: 'Desesperada para localizar una mujer desaparecida con Intel sobre Caspere, Frank se reúne con traficantes de droga mexicanos; Ani se infiltra en un círculo interno exclusivo, con Ray y Paul cubriendola.',
    stars: 4,
    price: 10,
    url_chapter: 'RxREyhPW5Tw',
    url_trailer: 'RxREyhPW5Tw',
    season_id: @tdseason2.id
},{
    chapterNumber: '2x07',
    title: 'Black Maps and Motel Rooms',
    poster: '24.jpg',
    synopsis: 'Mientras Ani, Paul, y Ray estan más cerca de encontrar la verdad acerca de Casper, las circunstancias se vuelven más peligrosas para todos ellos y todos a su alrededor. Frank trata con una traición interna, aprende información importante, y planea su siguiente movimiento.',
    stars: 5,
    price: 10,
    url_chapter: 'DB6txiz_W18',
    url_trailer: 'DB6txiz_W18',
    season_id: @tdseason2.id
},{
    chapterNumber: '2x08',
    title: 'Omega Station',
    poster: '24.jpg',
    synopsis: 'Ray y Ani intentan escapar de su situación desesperada, mientras que Frank ata algunos cabos sueltos.',
    stars: 4,
    price: 10,
    url_chapter: 'QucqzyEA44E',
    url_trailer: 'QucqzyEA44E',
    season_id: @tdseason2.id
}
])

Chapter.create([
{
    chapterNumber: '1x01',
    title: 'eps1.0_hellofriend.mov',
    poster: '30.jpg',
    synopsis: 'Un pirata informático notorio se interesa por el ingeniero de seguridad cibernética y vigilante-pirata informático Elliot; y una corporación del mal es hackeada.',
    stars: 5,
    price: 10,
    url_chapter: 'Ug4fRXGyIak',
    url_trailer: 'Ug4fRXGyIak',
    season_id: @mrseason1.id
},{
    chapterNumber: '1x02',
    title: 'eps1.1_ones-and-zer0es.mpeg',
    poster: '30.jpg',
    synopsis: 'Elliot se debate entre la aceptación de una oferta de trabajo de una empresa maligna y unirse al grupo de hackers FSociety.',
    stars: 4,
    price: 10,
    url_chapter: 'i4G5ljs5nX0',
    url_trailer: 'i4G5ljs5nX0',
    season_id: @mrseason1.id
},{
    chapterNumber: '1x03',
    title: 'eps1.2_d3bug.mkv',
    poster: '30.jpg',
    synopsis: 'Elliot intenta llevar una vida normal, pero no puede escapar de FSociety. Gideon, por su parte, empieza a sospechar; y Tyrell juega sucio.',
    stars: 4,
    price: 10,
    url_chapter: '--6sEpH5rGE',
    url_trailer: '--6sEpH5rGE',
    season_id: @mrseason1.id
},{
    chapterNumber: '1x04',
    title: 'eps1.3_da3m0ns.mp4',
    poster: '30.jpg',
    synopsis: 'Elliot se enfrenta a la abstinencia de morfina, teniendo una serie de alucinaciones sobre su vida. Angela hace un viaje por un camino que nunca ha estado antes.',
    stars: 4,
    price: 10,
    url_chapter: 'LBkEiQcEpVc',
    url_trailer: 'LBkEiQcEpVc',
    season_id: @mrseason1.id
},{
    chapterNumber: '1x05',
    title: 'eps1.4_3xpl0its.wmv',
    poster: '30.jpg',
    synopsis: 'Fsociety intenta penetrar Steel Mountain, la instalación de datos más segura en Estados Unidos.',
    stars: 4,
    price: 10,
    url_chapter: '5tFD-a6878E',
    url_trailer: '5tFD-a6878E',
    season_id: @mrseason1.id
},{
    chapterNumber: '1x06',
    title: 'eps1.5_br4ve-trave1er.asf',
    poster: '30.jpg',
    synopsis: 'Elliot intenta hackear a Vera fuera de la cárcel con el fin de salvar a alguien que le importa; el "juego" de Tyrell se vuelve loco; y Angela profundiza en la muerte de su madre.',
    stars: 5,
    price: 10,
    url_chapter: '6fZupnqycVQ',
    url_trailer: '6fZupnqycVQ',
    season_id: @mrseason1.id
},{
    chapterNumber: '1x07',
    title: 'eps1.6_v1ew-s0urce.flv',
    poster: '30.jpg',
    synopsis: 'Elliot extraña a Shayla. Angela hace un trato con Colby. Sharon Knowles tiene un encuentro con Tyrell.',
    stars: 4,
    price: 10,
    url_chapter: 'mmU5F5yPs_g',
    url_trailer: 'mmU5F5yPs_g',
    season_id: @mrseason1.id
},{
    chapterNumber: '1x08',
    title: 'eps1.7_wh1ter0se.m4v',
    poster: '30.jpg',
    synopsis: 'Allsafe es hackeado y Elliot se esta encontrando con White Rose. Tyrell es interrogado por la policía y Elliot se acuerda de quién es en realidad.',
    stars: 5,
    price: 10,
    url_chapter: 'ho8cMOTdf0c',
    url_trailer: 'ho8cMOTdf0c',
    season_id: @mrseason1.id
},{
    chapterNumber: '1x09',
    title: 'eps1.8_m1rr0r1ng.qt',
    poster: '30.jpg',
    synopsis: 'Elliot tiene que hacer frente a los retos de su pasado, y acostumbrarse a su nueva familia.',
    stars: 5,
    price: 10,
    url_chapter: 'kUVnZJk4XbI',
    url_trailer: 'kUVnZJk4XbI',
    season_id: @mrseason1.id
},{
    chapterNumber: '1x10',
    title: 'eps1.9_zer0-day.avi',
    poster: '30.jpg',
    synopsis: 'Otro gran descubrimiento para Elliot en torno a su familia y FSociety, el mundo de Tyrell empieza a cerrarse en torno a él y Angela tiene un visitante bastante inesperado.',
    stars: 5,
    price: 10,
    url_chapter: 'JpdXGe1AJUU',
    url_trailer: 'JpdXGe1AJUU',
    season_id: @mrseason1.id
}
])

Chapter.create([
{
    chapterNumber: '1x01',
    title: 'Currahee',
    poster: '31.jpg',
    synopsis: 'Easy Company pasa a través de la formación bajo la dirección de un capitán que implacablemente los empuja a sus límites, pero puede limitarse como un líder en el campo.',
    stars: 4,
    price: 10,
    url_chapter: 'Vwz5JT2KnT4',
    url_trailer: 'Vwz5JT2KnT4',
    season_id: @bobseason1.id
},{
    chapterNumber: '1x02',
    title: 'Day of Days',
    poster: '31.jpg',
    synopsis: 'Paracaidistas de la Easy Company saltan tras líneas enemigas en Normandía en el día D y luchan por reunirse en un territorio hostil.',
    stars: 5,
    price: 10,
    url_chapter: '8R0Cl22w8TA',
    url_trailer: '8R0Cl22w8TA',
    season_id: @bobseason1.id
},{
    chapterNumber: '1x03',
    title: 'Carentan',
    poster: '31.jpg',
    synopsis: 'Easy Company se enfrenta a tropas alemanas en la localidad francesa de Carentan, y la batalla se cobra su peaje en un soldado que está gravemente traumatizado por la experiencia.',
    stars: 5,
    price: 10,
    url_chapter: 'r7VU57n9NHg',
    url_trailer: 'r7VU57n9NHg',
    season_id: @bobseason1.id
},{
    chapterNumber: '1x04',
    title: 'Replacements',
    poster: '31.jpg',
    synopsis: 'Con la incorporación de nuevos hombres, Easy Company se dirige a Holanda para participar en la Operación Market Garden y preparar una ruta aliada en Alemania; pero se encuentran rígida resistencia alemana.',
    stars: 5,
    price: 10,
    url_chapter: 'mrR_XjaGT40',
    url_trailer: 'mrR_XjaGT40',
    season_id: @bobseason1.id
},{
    chapterNumber: '1x05',
    title: 'Crossroads',
    poster: '31.jpg',
    synopsis: 'El Capitán Winters conduce Easy Company en un ataque contra lo que pensaban que era un nido de ametralladora, pero resultan ser 2 Companias de la SS alemanas que también tienen apoyo de artillería. El ataque tiene éxito en cuanto a que prácticamente acaban con el enemigo, pero sufren muchas bajas.',
    stars: 5,
    price: 10,
    url_chapter: 'Uq5qRy-ErWA',
    url_trailer: 'Uq5qRy-ErWA',
    season_id: @bobseason1.id
},{
    chapterNumber: '1x06',
    title: 'Bastogne',
    poster: '31.jpg',
    synopsis: 'Es la Navidad de 1944, y la 101 está rodeado por los alemanes en el bosque fuera de Bastogne, con falta de suministros en el crudo frío y la nieve.',
    stars: 5,
    price: 10,
    url_chapter: 'R9vMMXbNKgg',
    url_trailer: 'R9vMMXbNKgg',
    season_id: @bobseason1.id
},{
    chapterNumber: '1x07',
    title: 'The Breaking Point',
    poster: '31.jpg',
    synopsis: 'La Easy Company permanece en el bosque de las Ardenas preparandose para un inevitable ataque de las fuerzas alemanas en la ciudad de Foy. Sin embargo, la moral esta baja debido al clima frío, los bombardeos constantes, la falta de liderazgo, y las numerosas bajas.',
    stars: 5,
    price: 10,
    url_chapter: 'SoNCeT_9RXU',
    url_trailer: 'SoNCeT_9RXU',
    season_id: @bobseason1.id
},{
    chapterNumber: '1x08',
    title: 'The Last Patrol',
    poster: '31.jpg',
    synopsis: 'Easy Company está en Hagenau en Febrebro de 1945, donde se preparan para una misión de patrulla nocturna para capturar prisioneros alemanes. La patrulla incluye un veterano que es despreciado por faltar a Bastogne y un nuevo teniente recién salido de West Point.',
    stars: 5,
    price: 10,
    url_chapter: 'QQ7JJGeItFg',
    url_trailer: 'QQ7JJGeItFg',
    season_id: @bobseason1.id
},{
    chapterNumber: '1x09',
    title: 'Why We Fight',
    poster: '31.jpg',
    synopsis: 'A medida que los aliados se mueven en Alemania y la guerra se acerca más a su fin, la desilusión y la ira establecida en la Easy Company - hasta que se topan con un campo de concentración abandonado por el ejército alemán.',
    stars: 5,
    price: 10,
    url_chapter: 'sHcJtU9dr6I',
    url_trailer: 'sHcJtU9dr6I',
    season_id: @bobseason1.id
},{
    chapterNumber: '1x10',
    title: 'Points',
    poster: '31.jpg',
    synopsis: 'A medida que los alemanes se rinden, parece que que los días duros de Easy Company han terminado, ya que están estacionados en Austria. Pero pronto aprenden que esas los soldados sin suficientes puntos de servicio serán enviados a luchar en Japón.',
    stars: 5,
    price: 10,
    url_chapter: 'A8qeIqfDa6A',
    url_trailer: 'A8qeIqfDa6A',
    season_id: @bobseason1.id
}
])

Chapter.create([
{
    chapterNumber: '1x01',
    title: 'Pilot',
    poster: '32.jpg',
    synopsis: 'Lucifer ha dejado infierno para asumir una vida en la Tierra. Cuando se asesina a un amigo suyo, Lucifer se une con el lado bueno de la ley para descubrir a los delincuentes y para darles lo que merecen por derecho.',
    stars: 5,
    price: 10,
    url_chapter: 'cZW9oLYlQtk',
    url_trailer: 'cZW9oLYlQtk',
    season_id: @lseason1.id
},{
    chapterNumber: '1x02',
    title: 'Lucifer, Stay. Good Devil.',
    poster: '32.jpg',
    synopsis: 'Cuando el hijo de una estrella de cine se mató después de ser perseguido por los paparazzi, Chloe hace una mirada profunda en el caso con un poco de ayuda de Lucifer. Mientras tanto, Maze y Amenadiel continúan alentando a Lucifer para volver al infierno.',
    stars: 4,
    price: 10,
    url_chapter: 'sNkANOk2mlg',
    url_trailer: 'sNkANOk2mlg',
    season_id: @lseason1.id
},{
    chapterNumber: '1x03',
    title: 'The Would-Be Prince of Darkness',
    poster: '32.jpg',
    synopsis: 'Un prometedor-y el quarterback llama Lucifer después de encontrar un cadáver en su piscina; Lucifer pide ayuda a Chloe para investigar, lo que los lleva al mundo de los deportes de mucho dinero.',
    stars: 4,
    price: 10,
    url_chapter: 'mGOtRIBYSUU',
    url_trailer: 'mGOtRIBYSUU',
    season_id: @lseason1.id
},{
    chapterNumber: '1x04',
    title: 'Manly Whatnots',
    poster: '32.jpg',
    synopsis: 'En un esfuerzo por superar su enamoramiento con Chloe, Lucifer decide que debe seducirla. Mientras tanto, los dos se unen en un caso de una niña desaparecida y Amenadiel enfrenta a Maze acerca de sus preocupaciones acerca de Lucifer.',
    stars: 4,
    price: 10,
    url_chapter: 't0Eq7T3I22o',
    url_trailer: 't0Eq7T3I22o',
    season_id: @lseason1.id
},{
    chapterNumber: '1x05',
    title: 'Sweet Kicks',
    poster: '32.jpg',
    synopsis: 'Cuando Lucifer está asistiendo a un desfile de moda, una chica es asesinado. A continuación, se ofrece como voluntario para ayudar a la detective Decker a resolver el homicidio. Mazikeen va detrás de la espalda de Lucifer.',
    stars: 4,
    price: 10,
    url_chapter: '3YWKkabQXhI',
    url_trailer: '3YWKkabQXhI',
    season_id: @lseason1.id
},{
    chapterNumber: '1x06',
    title: 'Favorite Son',
    poster: '32.jpg',
    synopsis: 'Un robo que salió mal conduce a Lucifer y Chloe a trabajar juntos. Dan tiene un encuentro inusual con Mazikeen. Chloe sospecha que Lucifer podría ser un criminal. Linda elige jugar el juego de Lucifer.',
    stars: 5,
    price: 10,
    url_chapter: 'euw81s3u7ps',
    url_trailer: 'euw81s3u7ps',
    season_id: @lseason1.id
},{
    chapterNumber: '1x07',
    title: 'Wingman',
    poster: '32.jpg',
    synopsis: 'Lucifer recibe ayuda de una fuente inesperada mientras trata de encontrar el contenido de su container robado; Chloe descubre una pista vital.',
    stars: 5,
    price: 10,
    url_chapter: '1tm1-ONFdhg',
    url_trailer: '1tm1-ONFdhg',
    season_id: @lseason1.id
},{
    chapterNumber: '1x08',
    title: 'Et Tu, Doctor?',
    poster: '32.jpg',
    synopsis: 'El asesinato de un terapeuta le pide a Lucifer contar con la ayuda del Dr. Linda para buscar a un sospechoso.',
    stars: 4,
    price: 10,
    url_chapter: '-3YZ2xNAShM',
    url_trailer: '-3YZ2xNAShM',
    season_id: @lseason1.id
},{
    chapterNumber: '1x09',
    title: 'A Priest Walks Into a Bar',
    poster: '32.jpg',
    synopsis: 'Un cura busca la ayuda de Lucifer cuando sospecha de una operación clandestina de drogas ha establecido una tienda en un centro de jóvenes del barrio. Mientras tanto, Malcolm manipula una forma de mantener un ojo en Dan.',
    stars: 5,
    price: 10,
    url_chapter: 'SSIEY8VpkAY',
    url_trailer: 'SSIEY8VpkAY',
    season_id: @lseason1.id
},{
    chapterNumber: '1x10',
    title: 'Pops',
    poster: '32.jpg',
    synopsis: 'Lucifer y Chloe sospechan que el hijo de un restaurador jugó un papel en su muerte; el regreso de la madre de Chloe pone su vida en agitación.',
    stars: 4,
    price: 10,
    url_chapter: 'yq95X-V_DrE',
    url_trailer: 'yq95X-V_DrE',
    season_id: @lseason1.id
},{
    chapterNumber: '1x11',
    title: 'St. Lucifer',
    poster: '32.jpg',
    synopsis: 'Cuando el filántropo Tim Dunlear es encontrado muerto, Lucifer explora su lado bueno al convertirse en un benefactor de Tim s glitzy Los Angeles charity.',
    stars: 5,
    price: 10,
    url_chapter: 'tThA5Si06Sg',
    url_trailer: 'tThA5Si06Sg',
    season_id: @lseason1.id
},{
    chapterNumber: '1x12',
    title: '#TeamLucifer',
    poster: '32.jpg',
    synopsis: 'El equipo investiga la muerte de una mujer cuyo cuerpo con el mensaje "Hail Lucifer" fue encontrado en un pentagrama.',
    stars: 5,
    price: 10,
    url_chapter: 'bjwtMDJlaOg',
    url_trailer: 'bjwtMDJlaOg',
    season_id: @lseason1.id
},{
    chapterNumber: '1x13',
    title: 'Take Me Back to Hell',
    poster: '32.jpg',
    synopsis: 'Cuando Lucifer es incriminado por asesinato, él y Chloe deben trabajar juntos para limpiar su nombre y demostrar la identidad del verdadero asesino.',
    stars: 5,
    price: 10,
    url_chapter: 'sqZGMLnTkWo',
    url_trailer: 'sqZGMLnTkWo',
    season_id: @lseason1.id
}
])

Chapter.create([
{
    chapterNumber: '1x01',
    title: 'AKA Ladies Night',
    poster: '33.jpg',
    synopsis: 'Jessica Jones es contratada para encontrar una bella estudiante de NYU que desapareció, pero resulta ser más que un simple caso de personas desaparecidas.',
    stars: 4,
    price: 10,
    url_chapter: 'nWHUjuJ8zxE',
    url_trailer: 'nWHUjuJ8zxE',
    season_id: @jjtemporda1.id
},{
    chapterNumber: '1x02',
    title: 'AKA Crush Syndrome',
    poster: '33.jpg',
    synopsis: 'Jessica se compromete a probar la inocencia de Hope, a pesar de que significa rastrear una figura aterradora de su propio pasado.',
    stars: 4,
    price: 10,
    url_chapter: 'nWHUjuJ8zxE',
    url_trailer: 'nWHUjuJ8zxE',
    season_id: @jjtemporda1.id
},{
    chapterNumber: '1x03',
    title: 'AKA Its Called Whiskey',
    poster: '33.jpg',
    synopsis: 'No va a ser fácil de adquirir o implementar, pero Jessica piensa que ha encontrado un arma para usar contra Kilgrave. Luke y Jessica estrechan relacion sobre sus similitudes.',
    stars: 4,
    price: 10,
    url_chapter: 'nWHUjuJ8zxE',
    url_trailer: 'nWHUjuJ8zxE',
    season_id: @jjtemporda1.id
},{
    chapterNumber: '1x04',
    title: 'AKA 99 Friends',
    poster: '33.jpg',
    synopsis: 'Un nuevo caso exige la atención de Jessica, mientras trata de averiguar quién la está espiando para Kilgrave. El programa de radio de Trish produce consecuencias inesperadas.',
    stars: 4,
    price: 10,
    url_chapter: 'nWHUjuJ8zxE',
    url_trailer: 'nWHUjuJ8zxE',
    season_id: @jjtemporda1.id
},{
    chapterNumber: '1x05',
    title: 'AKA The Sandwich Saved Me',
    poster: '33.jpg',
    synopsis: 'A pesar de las objeciones de Jessica, el nuevo amigo de Trish, Simpson,  se involucra en la búsqueda de Kilgrave. Jessica recuerda un momento crucial en su vida.',
    stars: 4,
    price: 10,
    url_chapter: 'nWHUjuJ8zxE',
    url_trailer: 'nWHUjuJ8zxE',
    season_id: @jjtemporda1.id
},{
    chapterNumber: '1x06',
    title: 'AKA You re a Winner!',
    poster: '33.jpg',
    synopsis: 'Luke contrata a Jessica para ayudar a encontrar a alguien que puede haberse ido de la ciudad, pero se ella teme que el va a aprender mucho acerca de su historia en el proceso.',
    stars: 4,
    price: 10,
    url_chapter: 'nWHUjuJ8zxE',
    url_trailer: 'nWHUjuJ8zxE',
    season_id: @jjtemporda1.id
},{
    chapterNumber: '1x07',
    title: 'AKA Top Shelf Perverts',
    poster: '33.jpg',
    synopsis: 'Malcolm, Simpson y Trish van sin escrúpulos para prevenir Jessica de llevar a cabo un plan extremo para ser mas lista que Kilgrave.',
    stars: 4,
    price: 10,
    url_chapter: 'nWHUjuJ8zxE',
    url_trailer: 'nWHUjuJ8zxE',
    season_id: @jjtemporda1.id
},{
    chapterNumber: '1x08',
    title: 'AKA WWJD?',
    poster: '33.jpg',
    synopsis: 'Jessica experimenta una extraña cortesía de Kilgrave. El conflicto de Hogarth con su ex esposa llega a un punto de inflexión.',
    stars: 5,
    price: 10,
    url_chapter: 'nWHUjuJ8zxE',
    url_trailer: 'nWHUjuJ8zxE',
    season_id: @jjtemporda1.id
},{
    chapterNumber: '1x09',
    title: 'AKA Sin Bin',
    poster: '33.jpg',
    synopsis: 'Justo cuando Jessica tiene Kilgrave justo donde ella lo quiere, la participación de Hogarth complica la situación. Los detalles del pasado de Kilgrave emergen.',
    stars: 5,
    price: 10,
    url_chapter: 'nWHUjuJ8zxE',
    url_trailer: 'nWHUjuJ8zxE',
    season_id: @jjtemporda1.id
},{
    chapterNumber: '1x10',
    title: 'AKA 1,000 Cuts',
    poster: '33.jpg',
    synopsis: 'Un descubrimiento tiene el potencial de cambiar el juego - Si Jessica puede rechazar la oferta de Kilgrave.',
    stars: 5,
    price: 10,
    url_chapter: 'nWHUjuJ8zxE',
    url_trailer: 'nWHUjuJ8zxE',
    season_id: @jjtemporda1.id
},{
    chapterNumber: '1x11',
    title: 'AKA I ve Got the Blues',
    poster: '33.jpg',
    synopsis: 'Jessica busca en los depósitos de cadáveres en busca de pistas. Trish hace todo lo posible para mantener Simpson fuera del camino de Jessica. Malcolm tiene una epifanía.',
    stars: 4,
    price: 10,
    url_chapter: 'nWHUjuJ8zxE',
    url_trailer: 'nWHUjuJ8zxE',
    season_id: @jjtemporda1.id
},{
    chapterNumber: '1x12',
    title: 'AKA Take a Bloody Number',
    poster: '33.jpg',
    synopsis: 'La búsqueda de Kilgrave reúne a Jessica con Luke. Trish recibe información inesperada sobre Simpson y Jessica.',
    stars: 5,
    price: 10,
    url_chapter: 'nWHUjuJ8zxE',
    url_trailer: 'nWHUjuJ8zxE',
    season_id: @jjtemporda1.id
},{
    chapterNumber: '1x13',
    title: 'AKA Smile',
    poster: '33.jpg',
    synopsis: 'Jessica y Luke reciben ayuda de otra persona en el vecindario. Kilgrave se prepara para una prueba importante de poderes contra de Jessica.',
    stars: 5,
    price: 10,
    url_chapter: 'nWHUjuJ8zxE',
    url_trailer: 'nWHUjuJ8zxE',
    season_id: @jjtemporda1.id
}
])