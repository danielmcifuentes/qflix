Rails.application.routes.draw do

  root 'welcome#index'

  # Home
  get 'principal/' => 'welcome#index', as: :welcome

  # Movies
  get 'peliculas/'        => 'movies#index', as: :movies
  get 'peliculas/generos' => 'movies#order', as: :movie_genres
  get 'peliculas/:id'     => 'movies#show',  as: :movie
  get 'peliculas/:id/play'    => 'movies#play',    as: :movie_play
  get 'peliculas/:id/trailer' => 'movies#trailer', as: :movie_trailer
  get 'peliculas/:id/comprar' => 'movies#buy', as: :movie_buy
  post 'peliculas/comprar' => 'user#buy', as: :user_buy

  # Series
  get 'series/'    => 'series#index', as: :series
  get 'series/:id' => 'series#show',  as: :serie
  get 'series/:id/trailer' => 'series#trailer', as: :serie_trailer
  post 'series/pagar' => 'series#pay', as: :serie_pay

  # Seasons
  get 'temporadas/'    => 'seasons#index', as: :seasons
  get 'temporadas/:id' => 'seasons#show',  as: :season
  get 'temporadas/:id/play'    => 'seasons#play',    as: :season_play
  get 'temporadas/:id/trailer' => 'seasons#trailer', as: :season_trailer
  post 'temporadas/pagar' => 'seasons#pay', as: :season_pay

  # Chapters
  get 'capitulos/'    => 'chapters#index', as: :chapters
  get 'capitulos/:id' => 'chapters#show',  as: :chapter
  get 'capitulos/:id/play'    => 'chapters#play',    as: :chapter_play
  get 'capitulos/:id/trailer' => 'chapters#trailer', as: :chapter_trailer
  post 'capitulos/pagar' => 'chapters#pay', as: :chapter_pay

  # Profile
  get 'perfil/' => 'user#show', as: :profile

  # Search
  get 'busqueda' => 'movies#search', as: :movie_search
  #get 'busqueda' => 'series#search', as: :serie_search


  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
