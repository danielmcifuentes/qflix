class Serie < ActiveRecord::Base

  has_many :seasons

  def self.search(search)
    where("lower(title) LIKE ? or lower(synopsis) LIKE ? or lower(genre) LIKE ?", "%#{search}%".downcase, "%#{search}%".downcase, "%#{search}%".downcase)
  end

end
