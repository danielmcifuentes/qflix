class Chapter < ActiveRecord::Base

  belongs_to :season

  def self.search(search)
    where("lower(title) LIKE ? or lower(synopsis) LIKE ? or lower(genre) LIKE ?", "%#{search}%".downcase, "%#{search}%".downcase, "%#{search}%".downcase)
  end

end
