
When(/^I go to my profile$/) do
  visit profile_path
end

Then(/^I am on my profile page$/) do
  expect(page.current_path).to eq(profile_path)
end

Then(/^I have a "([^"]*)" button$/) do |button|
  expect(page).to have_content(button)
end

Given(/^I have (\d+) QPoints$/) do |qp|
  @user.qpoints = qp.to_i
  @user.save!
end

When(/^I fill in "([^"]*)" with (\d+)$/) do |input, qp|
  expect(page.current_path).to eq(profile_path)
  expect(page).to have_selector("input[name='#{input}']")
  expect(page).to have_selector("button[id='buyqpoint']")
  fill_in input, :with => qp.to_i
end

Then(/^I should have (\d+) QPoints$/) do |qp|
  expect(page).to have_content("(#{qp} QP)")
end
