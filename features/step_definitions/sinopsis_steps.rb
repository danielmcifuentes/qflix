#encoding: utf-8

Given(/^an autenticated user "([^"]*)"$/) do |user|
  visit root_path
  expect(page).to have_content(user)
end

Given(/^a movie with title "([^"]*)" and synopsis "([^"]*)"$/) do |title, synopsis|
  @movie.title = title
  @movie.synopsis = synopsis
  @movie.save!
end


When(/^I go to movie's list$/) do
  visit movies_path
end

When(/^I click on movie's title$/) do
    expect(page).to have_content(@movie.title)
    visit movie_path(@movie.id)
end

Then(/^I see the sinopsis of the movie$/) do
  expect(page).to have_content(@movie.synopsis)
end


