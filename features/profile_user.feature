Feature: Ingresar al perfil del usuario logueado

  Scenario: Possibility of entering the user's profile
    Given I have logged in
    When I go to my profile
    Then I am on my profile page
     And I have a "Comprar QPoints" button
