Feature: Formulario de busuqeda

  Background:
    Given I have logged in
    And I see the navbar search form 

  Scenario Outline: Search movies by Title
    And I have a movie with "<title>" title, "<synopsis>" synopsis and "<genre>" genre
    When I fill the search input with "<title>"
    And I click the search button
    And I should see the text "Resultados de búsqueda para: <title>"
    And I should see the movie "<title>"

    Scenario Outline: Search movies by Synopsis
      And I have a movie with "<title>" title, "<synopsis>" synopsis and "<genre>" genre
      When I fill the search input with "<synopsis>"
      And I click the search button
      And I should see the text "Resultados de búsqueda para: <synopsis>"
      And I should see the movie "<title>"

    Scenario Outline: Search movies by Genre
      And I have a movie with "<title>" title, "<synopsis>" synopsis and "<genre>" genre
      When I fill the search input with "<genre>"
      And I click the search button
      And I should see the text "Resultados de búsqueda para: <genre>"
      And I should see the movie "<title>"

    Scenario Outline: Search for a movie that hasnt been uploaded
      And I have a movie with "<title>" title, "<synopsis>" synopsis and "<genre>" genre
      When I fill the search input with "La Masacre de Texas"
      And I click the search button
      And I should see the text "No se encontraron resultados de búsqueda para: La Masacre de Texas"


  Examples:
    | title             | genre           | synopsis                          |
    | Matrix            | Ciencia Ficción | Un pirata informático aprende...  |
    | QFlix - Release 1 | Programación    | Presentacion de funcionamiento... |
    | El padrino        | Drama           | Cuando el lider de una familia... |