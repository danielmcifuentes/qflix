Feature: Reproducir pelicula correspondiente al titulo indicado

  Background:
    Given I have logged in

  Scenario: Movie reproduction
    Given a movie called "Title"
      And synopsis "Synopsis"
      And movie "Movie"
     When I go to movies list
      And I click on movies title
     Then I see the full movie