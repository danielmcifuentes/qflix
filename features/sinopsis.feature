Feature: Pantalla de Sinopsis

  Background:
    Given I have logged in

  Scenario: Visual Content navigation
    Given a movie with title "Title" and synopsis "Synopsis"
     When I go to movie's list
      And I click on movie's title
     Then I see the sinopsis of the movie
	